#include "petscmat.h"  
#include "petscksp.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc,char* *argv){
	PetscErrorCode ierr;
	PetscViewer viewer;
        PetscMPIInt rank;
        PetscInt    rstartA,rendA,rstart,rend;
        Mat A,B,C;

	ierr=PetscInitialize(&argc,&argv,(char *)0,"");CHKERRQ(ierr); 
        ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
       
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"matrixA",FILE_MODE_READ,&viewer); 
	MatLoad(viewer,MATMPIAIJ,&A);
	PetscViewerDestroy(viewer); 

	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"matrixB",FILE_MODE_READ,&viewer); 
	MatLoad(viewer,MATMPIAIJ,&B);
	PetscViewerDestroy(viewer); 

        if (!rank) printf("A: \n");
	MatView(A,PETSC_VIEWER_STDOUT_WORLD);

        ierr = MatGetOwnershipRange(A,&rstartA,&rendA);
        ierr = MatGetOwnershipRange(B,&rstart,&rend);
        PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%d] A: %d - %d; B: %d - %d\n",rank,rstartA,rendA,rstart,rend);
        PetscSynchronizedFlush(PETSC_COMM_WORLD);
        if (!rank) printf("B: \n");
	MatView(B,PETSC_VIEWER_STDOUT_WORLD);
        
	ierr=MatMatMult(A,B,MAT_INITIAL_MATRIX,0.5,&C);CHKERRQ(ierr);
	if (!rank) printf("C: \n");
	MatView(C,PETSC_VIEWER_STDOUT_WORLD);
        
	MatDestroy(A);
	MatDestroy(B);
	MatDestroy(C);
	ierr = PetscFinalize();CHKERRQ(ierr);  
	return 0;
}
